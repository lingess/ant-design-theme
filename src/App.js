import React from "react";
import {Layout,Menu,Breadcrumb,Button,Divider,Switch,Checkbox,Input,DatePicker,Space,Slider,Alert,Radio,Upload} from "antd";
import { UploadOutlined } from '@ant-design/icons';


const fileList = [
    {
        uid: '-1',
        name: 'xxx.png',
        status: 'done',
        url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    },
    {
        uid: '-2',
        name: 'yyy.png',
        status: 'error',
    },
];
const { Header, Content, Footer } = Layout;

class App extends React.Component {
    changeTheme(e) {
        switch (e.target.value) {
            case 1:
                window.less
                    .modifyVars({
                        '@primary-color': '#676767',
                        '@success-color': '#97c561',
                        '@error-color': '#97c562'
                    })
                break
            case 2:
                window.less
                    .modifyVars({
                        "@primary-color": "#97c561",
                        '@success-color': '#97c561',
                        '@error-color': '#fff'
                    })
                break
            case 3:
                window.less
                    .modifyVars({
                        "@primary-color": "#97c561",
                        '@success-color': '#565665',
                        '@error-color': '#565665',
                        '@text-color': '#565665',
                        "@text-color-secondary": '#97c562'
                    })
                break
            case 4:
                window.less
                    .modifyVars({
                        "@primary-color": '#97c561',
                        '@text-color': '#565665',
                        "@text-color-secondary": '#14b9d6'
                    })
                break
            default:
                window.less
                    .modifyVars({
                        "@primary-color": "#565665"
                    })

        }
    };
    render() {
        return (
            <Layout className="layout">
                <Header>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={["2"]}
                    >
                        <Menu.Item key="1">nav 1</Menu.Item>
                        <Menu.Item key="2">nav 2</Menu.Item>
                        <Menu.Item key="3">nav 3</Menu.Item>
                    </Menu>
                </Header>
                <Content style={{ padding: "0 50px" }}>
                    <Breadcrumb style={{ margin: "16px 0" }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>
                    <div style={{ padding: 50 }}>
                        <div style={{ textAlign: "center" }}>
                            <Space>
                                <Radio.Group onChange={this.changeTheme}>
                                    <Radio value={1}>Theme 1</Radio>
                                    <Radio value={2}>Theme 2</Radio>
                                    <Radio value={3}>Theme 3</Radio>
                                    <Radio value={4}>Theme 4</Radio>
                                </Radio.Group>
                            </Space>
                        </div>
                        <Divider />
                        <div>
                            <Space>
                                <Button type="primary">Button</Button>
                                <Button type="link">Button</Button>
                                <Checkbox defaultChecked />
                                <Switch defaultChecked />
                                <Input />
                                <DatePicker />
                            </Space>
                        </div>
                        <div>
                            <Slider defaultValue={30} />
                        </div>
                        <Alert message="Informational Notes" type="info" showIcon />
                        <Alert message="Success Text" type="success" showIcon />
                        <Alert message="Warning Text" type="warning" showIcon />
                        <Alert message="Error Text" type="error" showIcon />
                        <Space>
                            <Upload
                                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                listType="picture"
                                defaultFileList={[...fileList]}
                                className="upload-list-inline"
                            >
                                <Button icon={<UploadOutlined />}>Upload</Button>
                            </Upload>
                        </Space>
                    </div>
                </Content>
            </Layout>
        )
    }
}

export default App;